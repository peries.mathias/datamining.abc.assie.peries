# Introduction

Ce projet a pour but d’effectuer une classification pour déterminer la sous-famille des différents domaines ABC. Il s’inscrit dans le cadre de l’UE Fouille de Données du Master 1 Bioinformatique et Biologie des Systèmes de l’Université Toulouse III - Paul Sabatier.

## Les systèmes ABC 

Les systèmes ABC, un acronyme d’ATP Binding Cassette, forment une très grande famille multigénique présente dans les trois règnes du domaine vivant. Ces systèmes sont des protéines transmembranaires, présentes dans toutes les cellules. Leur rôle majeur est le transport unidirectionnel de substrat au travers de la membrane cytoplasmique. Ce transport est dit “actif primaire” car il nécessite une source d'énergie produite par l’hydrolyse de l’ATP. 

Il existe deux types de transporteurs : 

- Les importeurs qui permettent l’entrée de molécules dans la cellule.
-  Les exporteurs qui permettent la sortie de molécules hors de la cellule.  

Ces systèmes sont composés de trois domaines différents qui ont chacun leur rôle. 
 
- Les domaines Nucleotide Binding Domain (NBD), correspondent à un site catalytique qui permet la liaison et l’hydrolyse de l’ATP. 
- Les domaines Membrane Spanning Domain (MSD), sont un site de liaison et de translocation du substrat. 
- Les domaines Substrate Binding Protein (SBP), un domaine présent uniquement chez les importeurs, sur la membrane des cellules du côté extracellulaire. Il permet la liaison du substrat à l’extérieur de la cellule et son acheminement jusqu’au domaine trans-membranaire. Ce domaine est retrouvé quasi exclusivement chez les procaryotes. 

Ces systèmes ABC sont présents chez toutes les espèces, ils sont donc très anciens. Au cours de l’évolution, les gènes codants pour ces systèmes ont subi de nombreuses duplications et spéciations, mais aussi accumulés des mutations. Leur fonction de transporteur est cependant préservée. Les protéines ABC peuvent être divisées en une vingtaines de sous-familles sur la base de la similarité de leurs séquences, mais aussi de la présence de domaines fonctionnels caractéristiques. En effet, de nombreux domaines fonctionnels spécifiques à ces systèmes ont été identifiés, ce sont les domaines GAF. 


![Transporteur ABC](transporteurs.png)

## La base de données ABCdb 
ABCdb est une banque de données publique développée et maintenue à Toulouse. Elle est consacrée aux protéines ABC des génomes procaryotes. Elle a été constituée à partir d’une centaine de génomes expertisés. Elle se décompose en deux sections : 

- CleanDb, pour les données vérifiées par un expert.
- AutoDb, où les données brutes de séquençage sont entrées et prédites automatiquement. 


# Analyse

## Les objectifs du projet 
Ce projet a pour objectif de répondre à une problématique actuelle. Une quantité importante de nouveaux génomes sont séquencés. Annoter et expertiser toutes ces séquences devient un réel défi car ce sont des tâches longues et fastidieuses. On cherche donc à réaliser l’annotation automatique de ces nouveaux génomes séquencés pour accéder rapidement à l’information, en attendant qu’ils soient expertisés manuellement. Pour ce faire, nous allons utiliser les données déjà expertisées afin de réaliser un modèle qui permet de prédire ces annotations. Pour un nouveau gène, il faut donc identifier si ce gène code pour une protéine du système ABC. S’il code pour un partenaire du système ABC, il faudra déterminer son architecture en domaines, la sous-famille à laquelle il appartient et enfin ses partenaires pour former le système.

## Les données fournies 

Les données à notre disposition sont neuf tables reliées entre elles : 

![Représentation des données](lesdonnées.PNG)


- La table *Taxonomy* contient la taxonomie du NCBI. Chaque taxon est annoté par un identifiant, un nom et un parent. Grâce à ce parent, on peut reconstruire un arbre. 

- La table *Strain* renseigne sur les génomes complets des organismes qui ont chacun un identifiant et un nom. Ils font partie d’un taxon, d’une espèce et d’une sous-espèce. Nous avons aussi l’information sur le sérotype de l’organisme qui correspond à l’ensemble des caractéristiques antigéniques de l’organisme et le pourcentage de GC. 

- La table *Chromosome* contient les différents chromosomes de chaque organisme. Chaque chromosome possède un identifiant, un type, une taille et le nombre de gènes qu’il contient codant pour une protéine. 

- La table *Gene* contient l’ensemble des gènes prédits et annotés dans le génome. Chaque gène possède un identifiant, l’identifiant du chromosome qui le porte, un nom, sa position de début et de fin sur le chromosome, le numéro du gène par rapport à l’origine de réplication, le score BLAST de l’alignement de la séquence du gène aligné sur sa séquence protéique, et sa fonction fournie par les fiches du génome annoté tel que EMBL ou Refseq. 

- La table *Conserved Domain* contient les résultats des analyses avec rpsblast pour détecter la présence de domaines sur l’ensemble des protéines. Chaque domaine conservé dans une protéine est identifié par l’identifiant du gène codant pour la protéine, et celui du domaine fonctionnel trouvé. La présence de ce domaine dans cette protéine est caractérisée par la position de début et de fin du domaine fonctionnel sur la protéine, respectivement Q_Start et Q_end, mais aussi par la position du début et de fin du domaine fonctionnel présent sur la protéine, respectivement S_Start et S_end. Le score et la E_Value du rpsblast sont également renseignés. 

- La table *Functional Domain* contient la description des domaines fonctionnels présents dans les banques de données publiques telles que CDD, COG, Pfam et SMART. Elle contient la description des domaines GAF qui sont les domaines fonctionnels spécifiques des protéines ABC. Chaque domaine fonctionnel est caractérisé par un identifiant du domaine fonctionnel FD_ID, un nom du domaine fonctionnel (FD_Name), la longueur de ce domaine ( FD_lenght) et sa description. Pour les domaines GAF spécifiques au domaine ABC, l’attribut Family_Link permet de faire le lien avec le type de domaine ABC et la sous-famille des transporteurs. 

- La table *Orthology* compile les résultats d’analyse BLAST pour l’identification entre paires d’orthologues 1:1. Ces prédictions ont été réalisées de manières automatiques.

- La table *Protein* contient les informations expertisées sur les séquences codant potentiellement pour les protéines ABC. Pour chaque protéine, on a l’identifiant du gène codant pour la protéine, la longueur de la séquence protéique, le type qui indique si la séquence est considérée comme codant pour un partenaire de système ABC, la sous-famille d’ABC attribuée par l’expert, le statut de l’identification, l’information sur l'intégrité de la séquence, l’identifiant du système ABC dans lequel la séquence est impliquée, la structure en domaine de la séquence protéique, le nombre de segments transmembranaires prédits et le type de peptide signal en début de séquence. 

- La table *Assembly* contient l’information sur les systèmes ABC reconstruits et expertisés. Chaque système ABC est identifié par l’identifiant du chromosome et l’identifiant du système, ou bien par un gène impliqué. La table contient aussi, pour chaque système, la composition en domaine et la sous-famille attribuée.



# Conception et Réalisation

## Notre objectif et les données pertinentes 

Notre objectif est de créer un classificateur avec un bonne qualité de prédiction afin de déterminer la sous-famille à laquelle appartiennent les protéines ABC. 

Pour ce faire , l’ensemble des tables fournies initialement ne sont pas nécessaires. Les tables que nous avons conservées sont la table Protein, Conserved Domain et Functional Domain. 

La table Protein contient les informations expertisées sur les protéines impliquées dans les systèmes ABC. En effet, nous allons faire en sorte de sélectionner seulement les protéines codant pour des partenaires ABC et qui ont un statut d’identification confirmé.  

La table Functional Domain regroupe les domaines fonctionnels spécifiques de systèmes ABC, les domaines GAF et la sous-famille qui lui est associée. Nous sélectionnerons donc uniquement les domaines GAF.

La table Conserved Domain regroupe les informations obtenues suite à l’alignement des protéines avec les domaines fonctionnels. 

## Réalisation d'un matrice - R 

*Ce travail a été intégralement réalisé sous RStudio et le fichier RMarkdown «projet_Assie_Peries.Rmd » est disponible dans le dossier "data.preparation".*

Premièrement, un filtrage sur la table Protein a été réalisé pour ne conserver que les protéines qui ont le statut d'identification “Confirmed”. Ce filtrage permet que seules les protéines correctement identifiées et expertisées soient conservées. Dans un second temps, la colonne Type est utilisée pour ne conserver que les lignes contenant le tag ABC ou SBP-like, afin de s’assurer que les protéines sont constitutives des transporteurs ABC. Enfin, un dernier filtrage grâce à la colonne Domain_Structure a été réalisé. Nous n’avons conservé que les lignes où seul un domaine est cité, afin de faciliter l’analyse future sur KNIME.

En deuxième étape, nous avons réalisé un filtrage sur la table Functionnal_Domain afin de ne conserver que les domaines fonctionnels correspondants à des domaines GAF qui sont spécifiques aux domaines présents dans les systèmes ABC. 
La troisième étape a consisté à supprimer toutes les colonnes dans les tables ne présentant pas d’intérêt pour la suite de l’analyse. Nous avons conservé uniquement les colonnes qui nous semblaient pertinentes : 

- FD_ID contenant les identifiants des domaines ABC fonctionnels identifiés. 

- Domain_Structure renseignant le type de domaine ABC identifié (MSD, NBD ou SBP). 

- Family_Link renseignant le type de domaine et la sous-famille de transporteur. 

- FD_Length qui correspond à la taille en acide-aminé du domaine fonctionnel. 

- Score correspondant au score calculé par rpsblast. Celui-ci nous renseigne sur l’homologie entre le domaine fonctionnel et la séquence protéique.   

Pour finir nous avons fusionné les différentes tables par le biais de la fonction merge. 

## La classification

La fouille de données est un processus visant à extraire des connaissances ou des motifs d’intérêt à partir d’une quantité importante de données. Il existe différentes méthodes afin de réaliser cela : la classification ou le clustering.
Nous nous concentrerons sur la classification. Cela consiste à examiner les caractéristiques d’un objet inconnu dans le but de lui attribuer une classe, de le catégoriser.

La classification se fait en deux étapes. Tout d’abord, il est nécessaire de construire un classificateur à partir d’un jeu de données d’apprentissage. Ce jeu de données contient des objets pour lesquels on connaît déjà la classe. 
Une fois le classificateur réalisé, deux cas de figure sont possibles. 

- Soit on cherche à prédire la classe de nouveaux objets dont on ne connaît pas la classe,

- Soit on cherche à estimer la précision de notre classificateur.  Pour ce faire, on utilise un jeu de données pour lequel on connaît déjà la classe.

Pour réaliser un classificateur et/ou une classification, différentes méthodes existent.  Nous réaliserons trois méthodes de classification : l’arbre de décisions, l’approche des plus proches voisins K-NN et la méthode bayésienne naïve. 
Pour estimer la précision de notre modèle, plusieurs moyens existent : 
le calcul de la spécificité qui est une mesure de la capacité à détecter les vrais positifs.
le calcul de la sensibilité qui est une mesure de la capacité de notre modèle à détecter un maximum de vrais positifs.
la Courbe ROC représentant les vrais positifs en fonction des faux positifs, mais on peut également réaliser un tableau.

### Arbre de décision 

La méthode d’apprentissage et de classification par arbre de décision est une méthode de classification supervisée. Elle permet de créer un classificateur. Elle peut être représentée par un arbre où les nœuds internes sont des tests effectués sur un des attributs, les branches les résultats des tests et les feuilles sont les différentes classes. 

Cette méthode est basée sur un algorithme glouton ou “greedy algorithm”. Cet algorithme permet de faire un choix localement, au niveau de chaque nœud, qui mène à un partage optimal. C’est une approche descendante récursive. A la racine, toutes les classes sont présentes. Au niveau de chaque nœud, l’attribut le plus pertinent pour diviser nos classes est sélectionné en calculant le gain de ratio de l’attribut, ou le coefficient de Gini. Ce sont deux approches différentes : le gain de ratio est basé sur la variance au sein des modalités de l'attribut, alors que le coefficient de Gini est basé sur la dispersion de la distribution de la population. Il est aussi possible de faire un simple test de X² d’indépendance. 

L’algorithme continue tant que les objets de classes différentes sont dans une même classe et que l’on a des attributs pour les départager.

### Bayésien naïf 

C’est un classificateur linéaire qui se base sur le théorème de Bayes et permet de prédire plusieurs hypothèses pondérées par leurs probabilités. 
 
![Principe Bayésien](Bayesien.PNG)

Il émet l’hypothèse que l'existence d’une caractéristique pour une classe est indépendante de l’existence d’autres caractéristiques. 
Il repose sur un algorithme d’apprentissage supervisé qui permet de classifier un ensemble d’observations selon les règles déterminées par l’algorithme lui-même. Son avantage est qu’il n’est pas nécessaire d’utiliser un grand nombre de données d'entraînement pour estimer les paramètres permettant la classification.  

### K Nearest Neighbors (*K-NN*)


La méthode K-NN représente chaque objet par un point dans un espace à n dimensions. Cet algorithme de machine learning appartient à la classe des algorithmes d’apprentissage supervisés non paramétriques. Il permet de déterminer  la classe d’une nouvelle donnée en fonction de celle de ses k plus proches voisins (k est défini par l’utilisateur). Les distances calculées utilisent les distances euclidiennes et de Mahalanobis qui prennent  en compte la variance statistique et la corrélation des données. 

Les étapes de cet algorithme sont : 

- Sélection du nombre de voisins k.

- Calcul d’une matrice de  distance.

- Regroupement des plus proches voisins en fonction du nombre k défini. 

- Parmi ces voisins k, comptage du nombre de points pour chaque catégorie. 

- Le nouveau point est attribué à la catégorie la plus présente dans ses k plus proches voisins. 

Cette méthode a quelques limites : la dimensionnalité des variables  fait que la distance peut être dominée par des attributs non pertinents. Il est donc nécessaire de normaliser les dimensions ou d’éliminer les attributs non pertinents. De plus, les données peuvent avoir des variances très hétérogènes, celles qui ont des variances trop basses seront écrasées. 

## KNIME
KNIME est un outil d’analyse et de préparation de données Open source dérivé de la plateforme de développement intégrée Eclipse. Ainsi, comme Eclipse, KNIME est développé en Java. Ce logiciel présente un grand nombre d’outils qui ne se cantonnent pas seulement à l’analyse biologique : il permet, entre autres, d’accéder à différentes sources de données, de les nettoyer, de les fusionner et de les enrichir et de les exporter, une fois mises en forme, vers un outil de visualisation de données. 

Grâce au logiciel Knime, nous allons créer trois classificateurs avec trois méthodes différentes de la même matrice. Ensuite, nous estimerons et comparerons leurs performances. Pour chaque classificateur, nous allons réaliser un workflow.

Dans chaque workflow réalisé, il y a des noeuds en commun : 

- Le nœud File Reader permet de lire la matrice préalablement créée. Il a été nécessaire de modifier le type de FD_ID à string car c’est une variable qualitative. 

- Le X-Partitionner permet de partitionner le jeu de données en un jeu d’apprentissage et un jeu de test. Plusieurs modes de partitionnement de données ont été réalisés : l'échantillonnage aléatoire de taille de 10 et 100, et le Leave-One-Out, afin de comparer les résultats.

- Le X-Aggregator permet d'agréger les résultats obtenus par la prédiction du jeu de test et et la classe connue. 

- Le nœud Scorer permet d’obtenir une matrice de confusion afin de comparer les classes prédites aux classes réelles. Il permet aussi d’obtenir la précision, la sensibilité et la spécificité pour chaque classe, mais aussi la précision de notre modèle. 

- Le noeud Statistics permet d’obtenir le taux d’erreur moyen, minimum et maximum

### Arbre de décision 

![ Workflow arbre de décision](ArbreDecision.png)

Le nœud Decision Tree Learner permet la création d’un classificateur grâce à la méthode de l’arbre de décision.  Dans ce nœud, nous pouvons choisir la mesure de qualité entre le score de Gini et le Ratio Gain, mais aussi choisir si l’on veut un élagage MDL ou aucun élagage.  
Le noeud Decision Tree Predictor permet d’utiliser le modèle du classificateur afin de classifier les données du jeu de test.

### Bayésien naïf 

![Workflow Bayésien naïf](WF_BN.png)

Le nœud Naive Bayes Learner permet la création d’un classificateur grâce à la méthode Bayésien naïf. 
Le nœud Naive Bayes predictor permet d’utiliser le modèle du classificateur afin de classifier les données du jeu de test.

### K Nearest Neighbors (*K-NN*)

![Workflow K-NN](WF_KNN.png)

Le nœud K Nearest Neighbor permet la création d’un classificateur grâce à la méthode de K-NN. Nous avons ajouté un nœud Normalizer pour normaliser nos données, afin de pouvoir comparer les résultats de ce classificateur aux résultats des autres classificateurs. En utilisant la méthode classique vue en TP pour effectuer un workflow K-NN sur KNIME, nous obtenions des erreurs car nous avions plusieurs variables qualitatives. Nous avons donc ajouté le nœud String Distances pour parer à ce problème comme KNIME nous le conseillait. 

# Interprétation et Discussion 

Pour rappel, 

- La sensibilité mesure la capacité de notre modèle à détecter un maximum de vrais positifs, 

- La spécificité mesure la capacité à détecter les vrais positifs, 

- La précision est la part des objets du jeu de test correctement classés. 

Pour chacune des méthodes de classification, ayant la spécificité, la sensibilité et la précision pour chaque classe, nous avons décidé de calculer la moyenne de chacun dee paramètres. Grâce au noeud scorer, nous avons obtenu la précision du modèle. Le nœud Statistics permet d’obtenir le taux d’erreur de nos classificateurs.


### Arbre de décision 

![Résultats Arbre Décision](Resultat_AD.png)

La précision moyenne, la sensibilité moyenne, la spécificité moyenne et la précision sont très proches de un, le taux d’erreur est autour de 5% ce qui signifie que notre modèle semble assez performant pour prédire la classe sous-famille.

Le risque de cette méthode est que l’arbre généré reflète trop le jeu d’apprentissage, ce qui rendrait la précision de notre modèle très faible pour de nouvelles données. C’est pour cela qu’on réalise un élagage MDL à posteriori. L’élagage ne diminue que très peu la précision de notre modèle. 
Le type de mesure de qualité, Gain ratio ou Gini index, ne semble pas impacter la précision.

### Bayésien naïf

![Résultats Bayésien naïf](Resultat_BN.png)

La précision moyenne, la sensibilité moyenne, la spécificité moyenne et la précision sont très proches de un,  ce qui signifie que notre modèle semble assez performant pour prédire la classe sous-famille, mais ces paramètres sont inférieurs à ceux du classificateur précédent. Ce classificateur  semble moins performant que celui issu de la méthode de l’arbre de décision.
La méthode de classification bayésien naïf se base sur l'hypothèse d’indépendance des données qui n’a, ici, pas été vérifiée. 

### K Nearest Neighbors (*K-NN*)

![Résultats K-NN ](Resultat_KNN.png)

La précision moyenne, la sensibilité moyenne, la spécificité moyenne et la précision sont très proches de un, ce qui signifie que notre modèle semble assez performant pour prédire la classe sous-famille. Aussi, c’est la méthode avec le taux d’erreur moyen le plus faible. Ces paramètres sont meilleurs que ceux des  classificateurs précédents. 


# Bilan et perspectives

La méthode K Nearest Neighbors (K-NN) semble la plus performante, elle donne le taux d’erreur le plus faible. Par ailleurs, nous avons rencontré quelques problèmes lors de l’élaboration des workflows des différentes méthodes de classification. Nous n’avons pas réussi à résoudre les problèmes sur le workflow de la méthode de random forest. 

Malgré les difficultés rencontrées au cours de l’élaboration de ce projet, nous sommes satisfaits du travail effectué, que ce soit vis à vis de l'élaboration de la matrice, qui donne des résultats tout à fait acceptables, ou les résultats de KNIME présentant un faible taux d’erreur. 

Le fait que vous nous ayez demandé de travailler avec un binôme différent aura été  une expérience extrêmement enrichissante. Nous avons pu, tous les deux, apprendre à nous connaître et à nous adapter à la  stratégie de travail de chacun. 

Pour le côté moins positif, le projet était complexe à appréhender. En effet, nous avons eu beaucoup de mal à déterminer quel serait le sujet de notre travail. Nous avions initialement décidé de prendre comme sujet l’une des perspectives que vous nous aviez proposées. Cependant, suite à l’envoi de la première matrice, que nous avions obtenu après de nombreux essais, vous nous aviez fait remarquer que nous voulions travailler sur le même sujet que certains de nos camarades. Aussi, bien qu’ayant réussi à faire une matrice fonctionnelle pour le sujet initial, nous avons préféré suivre votre conseil et changer de sujet. 

En ce qui concerne le choix des tables et des variables pour l’élaboration de la matrice, il a également été difficile de déterminer lesquelles seraient pertinentes pour notre analyse. Nous avons d’ailleurs, il faut l’admettre, produit un grand nombre de matrices différentes avant de choisir celle que nous allions utiliser sur KNIME. 

Pour ce qui est des améliorations, bien que nous soyons relativement satisfaits du travail réalisé, nous aurions pu essayer de commencer le projet plus tôt, de réfléchir plus longuement au but de notre analyse et aux données à conserver, avant de réaliser des matrices (nous en avons produit une cinquantaine). 

# Gestion du projet

Nous avons choisi de travailler chacun de manière autonome tout particulièrement pour la rédaction des différentes parties de ce rapport. Mais le gros du travail a été fait  en binôme, que ce soit par le biais de discord ou en présentiel en p0, notamment pour les parties sur KNIME et R. L’ensemble des décisions, que ce soit la conception de la matrice, les modèles utilisés sur KNIME ou bien le contenu du rapport ont été prises à deux, après discussion. 

Nous avons choisi et déterminé le travail que nous allions réaliser par le biais de discord, et de l’ensemble de tables et variables à conserver également.

Pour ce qui est du travail sur R et la réalisation de la matrice individu-variable : nous avons tous deux réalisé un certain nombre de matrices différentes avant de choisir la matrice finale que nous avons utilisée. Nous avons, pour cette partie, à la fois travaillé chacun de notre côté, et ensemble, en présentiel. De manière générale, c'est Mathias qui a le plus travaillé sur la réalisation de la matrice lorsque le travail a été réalisé en présentiel. Marine travaillait sur KNIME pour tester les différentes matrices réalisées. 

Le travail sur KNIME, comme dit précédemment, a plutôt été réalisé par Marine. De façon générale, nous avons fait cette partie en présentiel sur le PC personnel de Marine. 

Pour ce qui est de la rédaction du rapport, nous nous sommes répartis les différentes parties et, pour faciliter cette tâche, le travail a été réalisé sur un google doc. Dans les faits, Marine s’est occupée de la rédaction du contexte, de la partie analyse et celle sur KNIME. Mathias a rédigé l’introduction, la partie sur la création de la matrice sur R et la gestion de projet. Le reste a été réalisé à deux, tout comme le travail de relecture.  

Enfin, nous avons appris tardivement, le 1er mai, par le biais de Nadine, qu’il était nécessaire de vous rendre le projet via gitlab. De ce fait nous n’avons fait qu’ajouter le travail déjà réalisé sur le gitlab contrairement à ce que vous nous demandiez dans les consignes présentes sur votre gitlab. Nous avons tous deux contribué à sa création et son remplissage. 

![Gant](GANT.png)

Pour le Gant, les tâches bleues ciel correspondent aux étapes de rédaction, celles en bleu gris aux travaux sur R et Knime, celles en bleu foncé  aux discussions sur le sujet et aux tables à conserver, et enfin, en rouge, il y a la date d’envoi de la matrice. 
