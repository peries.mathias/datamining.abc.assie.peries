**Documentation utilisateur**


  Description de la création de la matrice individus-variables visant à être utilisées pour déterminer les sous familles de domaines ABC identifiés. 

**Les tables utilisées**

  Les tables initiales qui sont conservées sont : 
  - Protein 
  - Functionnal Domain
  - Conserved_Domain

Pour le bon fonctionnement du script R ces tables doivent être présentes dans le répertoire Data sur le gitlab. 

**Librairies nécessaires**

Quatre librairies R sont nécessaires, à savoir : 
  - dplyr
  - tidyr
  - stringr
  - data.table

**Chargement des tables**

L'ensemble de tables devront se trouvées dans le répertoir Data du gitlab au format .tsv. Elles seront chargées à partir de la fonction read.csv avec des tabulation comme séparateur. 
Le chemin peut être modifié au besoin par l'utilisateur. 

**Filtrage sur la table Protein**

La première étape consiste à filtrer les protéines pour : 
  - Ne conserver que celles qui ont le statut "confirmed"
  - Ne conserver que celles qui ont pour type 'ABC' ou 'SBP-like' car ce sont des partenaires ABC assurés. 
  - Ne conserver que celles avec un unique "domain structure", pour faciliter le travail sur KNIME. 
Enfin, seules les variables d'intérêts sont conservées, à savoir : Subfamily, Domain_Structure et Gene_ID

**Filtrage sur la table Functionnal Domain**

Cette seconde étape consiste, tout d'abord, à ne conserver que les domaines fonctionnels GAF, que l'on sait spécifiques aux ABC. 
Puis l'ensemble des lignes ayant un family link vide sont supprimées (pour ne conserver que les domaines avec une sous famille identifiée). 

**Suppression des variables inutiles**

Sur la table functionnal_domain seules les variables "FD_ID, FD_Length et Family_Link" seront conservées. Pour la table conserved_domain ne seront conservées que les variables " Gene_ID, FD_ID et Score". Les autres variables ne seront pas utile pour la suite. 

**Fusion des tables** 

Les tables conserved_domain et Protein sont fusionnées par un merge en fonction du Gene_ID. Cette nouvelle table et functionnal_domain sont ensuite fusionnées par un merge en fonction du FD_ID. 
A terme, une table finale "the_table" est obtenue. On ne conserve dans cette dernière que les variables : FD_ID, Family_Link, Domain_Structure, FD_Length et Score qui sont celles qui seront utiles à notre classificateur dans KNIME. 

**Enregistrement de la matrice individus-variables**

Enfin, la matrice indivus-variables est enregistrée au format .csv avec la fonction "write.table". 

